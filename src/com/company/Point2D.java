package com.company;

public class Point2D {
    protected double x, y;

    public Point2D(double x, double y){
        this.x = x;
        this.y = y;
    }

    public String toString(){
        return "x = " + x + ", y = " + y;
    }
}

package com.company;

public class Calculations {

    static public Point2D positionGeometricCenter(Point2D[ ] point){
        double temp1 = 0;
        double temp2 = 0;

        for(int i = 0; i < point.length; i++){
            temp1 += point[i].x;
            temp2 += point[i].y;
        }

        return new Point2D(temp1/point.length, temp2/point.length);
    }

    static public MaterialPoint2D positionCenterOfMass(MaterialPoint2D[ ] materialPoint){
        double temp1 = 0;
        double temp2 = 0;
        double mass = 0;

        for(int i = 0; i < materialPoint.length; i++){
            temp1 += materialPoint[i].getMass() * materialPoint[i].x;
            temp2 += materialPoint[i].getMass() * materialPoint[i].y;
            mass += materialPoint[i].getMass();
        }

        return new MaterialPoint2D(temp1/mass, temp2/mass, mass);
    }
}
